package com.hotel.booking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.booking.Model.Room;
import com.hotel.booking.Service.RoomService;

@RestController
@RequestMapping(value = "/room")
@CrossOrigin("*")
public class RoomController {
	
	@Autowired
	RoomService roomService;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Room> createRoom(@RequestBody Room room) {
		roomService.createRoom(room);
		return new ResponseEntity<>(room, HttpStatus.CREATED);
	}
	
	//@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Room> getRoom(@PathVariable long id) {
		Room room = roomService.getRoom(id).get();
		return new ResponseEntity<>(room, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Room> updateRoom(@PathVariable long id, @RequestBody Room roomUpdated) {
		Room oldRoom = roomService.getRoom(id).get();
		oldRoom.setPrice(roomUpdated.getPrice());
		oldRoom.setRoomNumber(roomUpdated.getRoomNumber());
		oldRoom.setStatus(roomUpdated.getStatus());
		roomService.updateRoom(oldRoom);
		return new ResponseEntity<>(oldRoom, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Long> deleteRoom(@PathVariable long id) {
		Room room = roomService.getRoom(id).get();
		roomService.delete(room);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}
	
	//@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Room>> getRooms() {
		List<Room> roomList = roomService.getRooms();
		return new ResponseEntity<>(roomList, HttpStatus.OK);
	}
}
