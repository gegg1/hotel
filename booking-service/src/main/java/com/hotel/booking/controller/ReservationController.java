package com.hotel.booking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.booking.Model.Reservation;
import com.hotel.booking.Model.Room;
import com.hotel.booking.Model.STATUS;
import com.hotel.booking.Service.ReservationService;
import com.hotel.booking.Service.RoomService;

@RestController
@RequestMapping(value = "/reservation")
@CrossOrigin("*")
public class ReservationController {
	
	@Autowired
	ReservationService reservationService;
	
	@Autowired
	RoomService roomService;
	
	//@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Reservation> booking(@RequestBody Reservation reservation) {
			reservation.setActive(true);
			reservationService.createReservation(reservation);
			return new ResponseEntity<>(reservation, HttpStatus.CREATED);
		
	}
	
	//@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Reservation> getReservation(@PathVariable Long id) {
		Reservation reservation = reservationService.getReservation(id).get();
		return new ResponseEntity<>(reservation, HttpStatus.OK);
	}
	
	//@CrossOrigin
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Reservation> updateReservation(@PathVariable Long id, @RequestBody Reservation reservation) {
		Reservation r = reservationService.getReservation(id).get();
		System.out.println(r.toString());
		r.setStartDate(reservation.getStartDate());
		r.setEndDate(reservation.getEndDate());
		reservationService.updateReservation(r);
		System.out.println(r.toString());
		return new ResponseEntity<>(r, HttpStatus.OK);
	}
	
	//@CrossOrigin
	@RequestMapping(value = "/checkout/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Reservation> cancelReservation(@PathVariable Long id) {
		Reservation reservation = reservationService.getReservation(id).get();
		Room room = roomService.getRoom(reservation.getRoomId()).get();
		room.setStatus(STATUS.UNOCCUPIED);
		roomService.updateRoom(room);
		reservation.setActive(false);
		reservationService.updateReservation(reservation);
		return new ResponseEntity<>(reservation, HttpStatus.OK);
	} 
	
	//@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Reservation>> getReservations() {
		return new ResponseEntity<>(reservationService.getReservations(), HttpStatus.OK);
	}
	 

}
