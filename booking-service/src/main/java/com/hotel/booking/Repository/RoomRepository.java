package com.hotel.booking.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotel.booking.Model.Room;

public interface RoomRepository extends JpaRepository<Room, Long> {

}
