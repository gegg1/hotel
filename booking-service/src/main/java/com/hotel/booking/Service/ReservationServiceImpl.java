package com.hotel.booking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotel.booking.Model.Reservation;
import com.hotel.booking.Repository.ReservationRepository;

@Service
public class ReservationServiceImpl implements ReservationService {
	
	@Autowired
	ReservationRepository reservationRepository;

	@Override
	public Reservation createReservation(Reservation reservation) {
		return reservationRepository.save(reservation);
	}

	@Override
	public Optional<Reservation> getReservation(long reservationId) {
		return reservationRepository.findById(reservationId);
	}

	@Override
	public Reservation updateReservation(Reservation reservation) {
		return reservationRepository.save(reservation);
	}

	@Override
	public void delete(Reservation reservation) {
		reservationRepository.delete(reservation);
	}

	@Override
	public List<Reservation> getReservations() {
		return reservationRepository.findByActive(true);
	}
	
}
