package com.hotel.booking.Service;

import java.util.List;
import java.util.Optional;

import com.hotel.booking.Model.Reservation;

public interface ReservationService {
	
	Reservation createReservation(Reservation reservation);
	
    Optional<Reservation> getReservation(long reservationId);
	
    Reservation updateReservation(Reservation reservation);
	
	void delete(Reservation reservation);
	
	List<Reservation> getReservations();

}
