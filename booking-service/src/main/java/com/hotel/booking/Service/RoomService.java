package com.hotel.booking.Service;

import java.util.List;
import java.util.Optional;

import com.hotel.booking.Model.Room;

public interface RoomService {
	
	Room createRoom(Room room);
	
    Optional<Room> getRoom(long roomId);
	
	Room updateRoom(Room roomUpdated);
	
	void delete(Room room);
	
	List<Room> getRooms();

}
