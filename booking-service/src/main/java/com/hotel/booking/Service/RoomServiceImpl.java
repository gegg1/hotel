package com.hotel.booking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotel.booking.Model.Room;
import com.hotel.booking.Repository.RoomRepository;

@Service
public class RoomServiceImpl implements RoomService {
	
	@Autowired
	RoomRepository roomRepository;

	@Override
	public Room createRoom(Room room) {
		return roomRepository.save(room);
	}

	@Override
	public Optional<Room> getRoom(long roomId) {
		return roomRepository.findById(roomId);
	}

	@Override
	public Room updateRoom(Room roomUpdated) {
		return roomRepository.save(roomUpdated);
	}

	@Override
	public void delete(Room room) {
		roomRepository.delete(room);
	}

	@Override
	public List<Room> getRooms() {
		return roomRepository.findAll();
	}

}
