export class Room {

    id: number;
    roomNumber: number;
    status: string;
    price: number;

    constructor(id: number, roomNumber: number, status: string, price: number) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.status = status;
        this.price = price;
    }

}
