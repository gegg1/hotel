import { Customer } from "./customer";

export class Reservation {

    customer: Customer;
    startDate: string;
    endDate: string;
    roomId: number;

    constructor(customer: Customer, startDate: string, endDate: string, roomId: number) {
        this.customer = customer;
        this.startDate = startDate; 
        this.endDate = endDate;
        this.roomId = roomId;
    }
}
