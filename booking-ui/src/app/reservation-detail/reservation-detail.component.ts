import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { BookingService } from '../booking.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Customer } from '../classes/customer';
import { Reservation } from '../classes/reservation';

@Component({
  selector: 'app-reservation-detail',
  templateUrl: './reservation-detail.component.html',
  styles: [
  ]
})
export class ReservationDetailComponent implements OnInit {

  reservationSubscription: Subscription | any;
  reservation: any = {};
  customerName: any;
  customerLastName: any;
  isSaved = false;
  message: string = '';
  reservationList: any[] = [];
  datesReservation: Set<string> = new Set<string>();
  startDateMessage: string = '';
  endDateMessage: string = '';
  reservationDatesMessage: string = '';

  constructor(private route: ActivatedRoute, private bookingService: BookingService) { }

  ngOnInit(): void {
    let reservationId: any = this.route.snapshot.paramMap.get('id');
    this.reservationSubscription = this.bookingService.getReservation(reservationId)
    .subscribe((res: any[]) => {
      this.reservation = res;
      this.customerName = this.reservation.customer.name;
      this.customerLastName = this.reservation.customer.lastName;
    });

    this.reservationSubscription = this.bookingService.getReservations().subscribe((res: any[]) => 
    {
      this.reservationList = res;
      this.datesReservation = this.getAllDates(this.reservationList);
    });
  }

  bookingForm = new FormGroup({
    name: new FormControl('',),
    lastName: new FormControl('',),
    startDate: new FormControl('', [Validators.required, this.startDateValidator.bind(this), this.datesValidator.bind(this)]),
    endDate: new FormControl('', [Validators.required, this.endDateValidator.bind(this), this.datesValidator.bind(this)])
  });

  getAllDates(reservationList: any[]): Set<string> {
    let dates = new Set<string>();
    reservationList.forEach((reservation) => {
      dates.add(reservation.endDate);
      dates.add(reservation.startDate);
    });
    return dates;
  }

  validateInterval(dateToValidate: Date): boolean {
    let reservationId: any = this.route.snapshot.paramMap.get('id');
    for (let reservation of this.reservationList) {
      let startDate: Date = new Date(reservation.startDate.replace(/-/g, '\/'));
      let endDate: Date = new Date(reservation.endDate.replace(/-/g, '\/'));
      if (dateToValidate >= startDate && dateToValidate <= endDate) {
        if (reservationId == reservation.id) {
          continue;
        } else {
          return true;
        }
      }
    }
    return false;
  }

  handleBookRoom(): void {
    let customer: Customer = new Customer(this.bookingForm.controls['name'].value,
      this.bookingForm.controls['lastName'].value);
    let startDate: string = this.bookingForm.controls['startDate'].value;
    let endDate: string = this.bookingForm.controls['endDate'].value;
    let reservation: Reservation = new Reservation(customer, startDate, endDate, 1);
    let params: String = JSON.stringify(reservation);
    let reservationId: any = this.route.snapshot.paramMap.get('id');
    this.bookingService.updateReservation(reservationId ,params)
      .subscribe((res: any) => {

        if (res && res.id) {
          this.isSaved = true;
        }
      });

  }

  startDateValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value) {
      let startDate: Date = new Date(control.value.replace(/-/g, '\/'));
      let today: any = new Date();
      if (startDate <= today) {
        this.startDateMessage = 'Start date cannot be lower than today or today';
        return { 'alert': true }
      }
    }
    return {};
  }

  endDateValidator(control: FormControl): { [s: string]: boolean } {

    if (control.value) {
      let endDate: Date = new Date(control.value.replace(/-/g, '\/'));
      let maxDate: Date = new Date();
      maxDate.setDate(maxDate.getDate() + 30);
      if (endDate > maxDate) {
        this.endDateMessage = 'Cannot place a reservation more than 30 days in advance';
        return { 'alert': true }
      }
    }
    return {};
  }

  datesValidator(control: FormControl): { [s: string]: boolean } {

    if (control.value) {  

      let dateToValidate: Date = new Date(control.value.replace(/-/g, '\/')); 
      if (this.validateInterval(dateToValidate)) {
        this.reservationDatesMessage = 'There is already a reservation on this date, pls select a different day';
        return { 'alert': true }
      }
      /*   
      if (this.datesReservation.has(control.value)) {
        this.reservationDatesMessage = 'There is already a reservation on this date, pls select a different day';
        return { 'alert': true }
      }
      */
    }
    return {};
  } 

  getDifferenceInDays(date1: Date | any, date2: Date | any) {
    const diffInMs = Math.abs(date2 - date1);
    return diffInMs / (1000 * 60 * 60 * 24);
  }

}
