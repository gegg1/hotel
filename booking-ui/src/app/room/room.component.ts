import { Component, OnInit } from '@angular/core';
import { Room } from '../classes/room';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styles: [
  ]
})
export class RoomComponent implements OnInit {

  roomList: any[] = [];
  room : Room | undefined;

  constructor(private roomService : RoomService) { }

  ngOnInit(): void {
    this.roomService.getRooms().subscribe((data : any) => {
      this.roomList = data;
    });
  }
 
}
