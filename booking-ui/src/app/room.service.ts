import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Room } from './classes/room';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  //bookingServiceUrl : string = "http://localhost:8081/"
  bookingServiceUrl : string = "http://54.198.143.244:8081/"
  

  constructor(private http : HttpClient) {}

  getRooms() : Observable<any> {
    return this.http.get<any>(`${this.bookingServiceUrl}room`)
  }

}
