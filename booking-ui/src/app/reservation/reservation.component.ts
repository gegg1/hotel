import { Component, OnInit } from '@angular/core';
import { BookingService } from '../booking.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styles: [
  ]
})
export class ReservationComponent implements OnInit {

  reservationList: any[] = [];
  reservationSubscription: Subscription | any;

  constructor(private bookingService: BookingService, private router: Router) { }

  ngOnInit(): void {
    this.reservationSubscription = this.bookingService.getReservations()
    .subscribe((res: any[]) => {
      this.reservationList = res;
    });
  }

  handledCancelReservation(reservation: any): any {
    if(confirm("Are you sure to cancel the reservation??")) {
      this.bookingService.cancelReservation(reservation.id)
      .subscribe((res: any) => {
      });
      this.router.navigate(['room']);
    }
  }

}
