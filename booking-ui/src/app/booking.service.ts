import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  createReservation(formData: any): any {
    let headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8', 'Accept': '*/*'});
    
    return this.http.post('http://54.198.143.244:8081/reservation', formData, {headers: headers})
      .pipe(map((res: any) => {
        return res;
      }));
  }

  updateReservation(reservationId: any, formData: any): any {
    let headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8', 'Accept': '*/*'});
    
    return this.http.put('http://54.198.143.244:8081/reservation/' + reservationId, formData, {headers: headers})
      .pipe(map((res: any) => {
        return res;
      }));
  }

  getReservations() : Observable<any> {
    return this.http.get<any>('http://54.198.143.244:8081/reservation');
  }

  getReservation(reservationId: any) : Observable<any> {
    return this.http.get<any>('http://54.198.143.244:8081/reservation/' + reservationId);
  }

  cancelReservation(reservationId: any): any {
    console.log('inside service')
    console.log('reservationid: ' + reservationId);

    return this.http.put(`http://54.198.143.244:8081/reservation/checkout/${reservationId}`, null)
    .pipe(map((res: any) => {
      console.log(res);
      return res;
    }));
  }


}
