import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookRoomComponent } from './book-room/book-room.component';
import { ReservationDetailComponent } from './reservation-detail/reservation-detail.component';
import { ReservationComponent } from './reservation/reservation.component';
import { RoomComponent } from './room/room.component';

const routes: Routes = [
  { path: 'room', component: RoomComponent },
  { path: 'room/new', component: BookRoomComponent },
  { path: 'reservations', component: ReservationComponent },
  { path: 'reservations/:id', component: ReservationDetailComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
